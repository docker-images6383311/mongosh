FROM docker.io/library/node:lts-alpine

LABEL org.opencontainers.image.title="zwieratko MongoDB Shell"
LABEL org.opencontainers.image.authors="zwieratko1@gmail.com"
LABEL org.opencontainers.image.source="https://gitlab.com/docker-images6383311/mongosh"
LABEL org.opencontainers.image.licenses="MIT"

WORKDIR /tmp

RUN <<EOF
apk upgrade --no-cache
npm install -g mongosh
npm cache clean --force
EOF

# USER node

CMD ["/bin/sh"]
