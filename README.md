# mongosh

Alpine Linux Docker Image with preinstalled packages:

- [mongosh](https://www.mongodb.com/docs/mongodb-shell/)
- [NodeJS](https://nodejs.org/en)
- [npm](https://docs.npmjs.com/about-npm)
- [yarn](https://yarnpkg.com/getting-started)
